# TinyTinyRSS

This role installs TinyTinyRSS on a dedicated host.

The TTRSS project strongly recommends users install it using their prebuilt
Docker image, and I do too.
If you are at all unfamiliar with TTRSS, or not sure what you are doing,
you should follow the recommendation and install using Docker, if for no other
reason, than to be able to expect support from other TTRSS users and developers.

I hope this is not hubris on my part, but I feel confident I can manage TTRSS
installed directly without using the Docker image.
This is mainly driven by my personal preference of LXC containers over Docker 
containers.

I wrote this Ansible role in order to migrate my TTRSS instance from a bare-metal
host (where it served me faithfully for years) to an LXC container.

## Requirements

+ I remember reading somewhere that TTRSS recommended PostgreSQL over MySQL.
+ PHP (at least v7.1)

## Other Ansible roles for TinyTinyRSS

I have not found any other Ansible roles for TTRSS (not using Docker, that is).


## Android

TTRSS's F-Droid repository which served us well for a number of years
[is no more](https://community.tt-rss.org/t/f-droid-repository-for-tt-rss/1321).

The app should auto-update? Not confirmed...


## Refs

+ https://tt-rss.org/wiki/InstallationNotesHost
+ https://tt-rss.org/wiki/PhpCompatibilityNotes
+ https://tt-rss.org/wiki/Plugins
