# Migration from armant to taipei

Migrating from `armant`:
```
taha@armant:~/public/ttrss
$ git log -1
commit 05744bb474e55c4f85acd236dd2883679c278e70 (origin/master, origin/HEAD)
Author: Andrew Dolgov <noreply@fakecake.org>
Date:   Tue Sep 22 20:33:51 2020 +0300
```

Quite out-dated, as you can see.
`armant` is a bare-metal host running Ubuntu 18.04, PHP 7.3.27 and PostgreSQL 10.19.


Migrating to LXC container `taipei` running Ubuntu 20.04, PHP 7.3.33 and PostgreSQL 12.10.


## On the source

Stop apache site (to avoid traffic during the migration.
```
taha@luxor:~
$ sudo a2dissite ttrss.chepec.se.conf
$ sudo systemctl reload apache2.service
```

Likewise, stop the updater daemon:
```
taha@armant:~
$ sudo systemctl stop ttrss.service
```

Dump the database.
```
taha@armant:~
$ sudo su - postgres
postgres@armant:~$ pg_dump -U postgres -W -d ttrss > ttrss.sql
```

This writes the database to `/var/lib/postgresql/ttrss.sql` (the `postgres` user home directory).


## On the target

Delete the database:
```
taha@taipei:~
$ sudo su - postgres
postgres@taipei:~$ psql
postgres=# drop database ttrss;
```

Run this role with `ttrss_repo.version: 05744bb474e55c4f85acd236dd2883679c278e70`
(same as source) and make sure `update_schema: false`:
```
taha@asks2:/media/bay/taha/projects/ansible/playbooks/luxor
$ ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --limit taipei --tags ttrss
```

Import the migrated database:
```
taha@taipei:~
$ sudo -u postgres psql ttrss < ~/Downloads/ttrss.sql
```

(this prints a lot of SQL return codes, but completes within less than a minute or so).

Attempted to start the update daemon systemd service. Fails. Huh.
Seems the config syntax we are using does not work with this older version of TTRSS.

Let's replace the config file with the one from the source.
Note that the line `define('DB_HOST', '');` had to be replaced with 
`define('DB_HOST', 'localhost');` to avoid the error `Peer auth failed for user ttrss`.

Also, since we copied our original config, also need to reset `SELF_URL_PATH`!

OK, migrated instance works. Everything looks normal.


### Upgrade TTRSS to latest available commit

... and re-create `config.php` using the new syntax.

Set `ttrss_db.update_schema: true`, set `ttrss_repo.version: "latest"`,
and ran this:
```
taha@asks2:/media/bay/taha/projects/ansible/playbooks/luxor
$ ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --limit taipei --tags ttrss-install
$ ansible-playbook playbook-containers.yml --ask-become-pass --ask-vault-pass --limit taipei --tags ttrss-plugins
```

OK, that was painless enough.


### Plugins require some manual work

Although all our plugins have been installed, or even enabled as in the case
of Wallabag, their buttons are not visible in each article.

Turns out the Wallabag plugin was missing [this fix](https://github.com/joshp23/ttrss-to-wallabag-v2/issues/44).
Applied it (manually), and the plugin works fine now.

The Shaarli plugin was also outdated. One of its forks had more recent commits,
so I switched to it. But the fork has cosmetic issues. Nonetheless, it's working.



### Other manual adjustments

I like to disable purging of articles, so I set 
`Preferences -> Articles -> Purge articles older than` to `0`.

The body text colour in all articles has changed from black to a shade of gray.
I don't like it all - makes it harder to read!

This is a theme issue. Will have to look into it.
Of all the already installed themes, only `light-high-contrast` fixed this issue
without changing too much other things. There's only one downside with this theme,
and that's that it looks a little severe in places (contrast is too high for my 
taste, and in some places the font looks like it's not properly anti-aliased or
something).

Let's edit the CSS of the default theme. Simple change the body text colour to black.
Fired up the web inspector, and found this:
```
# line 1316 in light.css
body.ttrss_main {
   color: #555;
   font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
   font-size: 14px;
}
```

Let's add the following to the `Customize stylesheet` setting in `Preferences`:
```
.ttrss_main {
   color: #000 !important;
}
```

I'm not sure why `!important` was necessary, but all other variations I tried
did not take effect. This works.

+ https://stackoverflow.com/a/21297248
+ https://git.tt-rss.org/fox/tt-rss/src/branch/master/index.php#L128
