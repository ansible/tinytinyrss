---

# critical for the postgresql_user and postgresql_db plugins to work as expected
- name: "Make sure access control list (ACL) utilities are installed"
  ansible.builtin.apt:
    name: acl
    state: present

# In pgsql parlance, a user is a role with login privilege
- name: "Create pgsql database user {{ ttrss_db.user }}"
  community.postgresql.postgresql_user:
    name: "{{ ttrss_db.user }}"
    password: "{{ ttrss_db.pwd }}"
    # https://dba.stackexchange.com/a/263911
    role_attr_flags: CREATEDB,CREATEROLE,INHERIT,NOSUPERUSER,LOGIN
    encrypted: yes
    state: present
    expires: infinity
    port: "{{ ttrss_db.port | int }}"
  become: true
  # db_admin_username is defined in group_vars and/or postgres/defaults
  become_user: "{{ db_admin_username }}"

# check which pgsql databases exist
- name: Collect info on all existing pgsql databases
  community.postgresql.postgresql_info:
    filter:
      - "databases"
  become: true
  become_user: "{{ db_admin_username }}"
  register: ttrss_pgsql_info

- name: "Create empty database {{ ttrss_db.name }}"
  community.postgresql.postgresql_db:
    name: "{{ ttrss_db.name }}"
    encoding: UTF-8
    state: present
    owner: "{{ ttrss_db.user }}"
    port: "{{ ttrss_db.port | int }}"
  become: true
  become_user: "{{ db_admin_username }}"
  when: "ttrss_db.name not in ttrss_pgsql_info.databases"
  register: ttrss_pgsql_database

- name: "Grant pgsql user {{ ttrss_db.user }} ALL privileges on database {{ ttrss_db.name }}"
  community.postgresql.postgresql_user:
    db: "{{ ttrss_db.name }}"
    priv: ALL
    name: "{{ ttrss_db.user }}"
    password: "{{ ttrss_db.pwd }}"
    encrypted: yes
    state: present
    expires: infinity
    port: "{{ ttrss_db.port | int }}"
  become: true
  become_user: "{{ db_admin_username }}"
